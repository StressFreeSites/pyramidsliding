'use strict';

exports.handler = (event, context, callback) => {
    var numberOfLayers = event.queryStringParameters.numberOfLayers,
    	numberOfItemsInRow = 1;

    var network = [],
    	shortestPathNetwork = [];

    if(typeof numberOfLayers == 'undefined') {
        callback(null, {
            statusCode: '400',
            body: JSON.stringify({
                "error": 'The numberOfLayers parameter must be defined'
            }),
            headers: {
                'Content-Type': 'application/json',
            },
        });
    }

    // Creating a pyramid with populated with random numbers
    for(var i = 0; i < numberOfLayers; i++) {
    	network[i] = [];
    	shortestPathNetwork[i] = [];

    	for(var j = 0; j < numberOfItemsInRow; j++) {
    		network[i][j] = Math.floor(Math.random() * 10 + 1)
    		shortestPathNetwork[i][j] = -1;
    	}

    	numberOfItemsInRow++;
    }

    //console.log(network);

    var start = process.hrtime();

    // Passing through the 2D array calculating the shortest path to each node
    for(var i = 0; i < (network.length - 1); i++) {
	    var currentRow = network[i];

	    for(var j = 0; j < currentRow.length; j++) {
    		var currentNode = network[i][j];

    		// Case to define the shortest path for the initial node
    		if(shortestPathNetwork[i][j] == -1) {
    			shortestPathNetwork[i][j] = currentNode;
    		}

    		// Checking if path to left descending node is shorter than currently calculated path
    		var shortestpathToLeftDesendent = shortestPathNetwork[i][j] + network[i+1][j];
    		if(shortestPathNetwork[i+1][j] == -1
	    		|| shortestpathToLeftDesendent < shortestPathNetwork[i+1][j]) {
	    		shortestPathNetwork[i+1][j] = shortestpathToLeftDesendent;
	    	}

	    	// Checking if path to right descending node is shorter than currently calculated path
	    	var shortestpathToRightDesendent = shortestPathNetwork[i][j] + network[i+1][j+1];
	    	if(shortestPathNetwork[i+1][j+1] == -1
	    		|| shortestpathToRightDesendent < shortestPathNetwork[i+1][j+1]) {
	    		shortestPathNetwork[i+1][j+1] = shortestpathToRightDesendent;
	    	}
    	}
    }

    // If a network exsists, finding the value of the shortest path
    // by scanning last row in the pyramid
    if(network.length > 0) {
        var lastRow = network[network.length-1],
        	shortestPath = -1;

            for(var j = 0; j < lastRow.length; j++) {
            	var currentShortestPathNode = shortestPathNetwork[i][j];

            	if(shortestPath == -1
            		|| currentShortestPathNode < shortestPath) {
        		shortestPath = currentShortestPathNode;
        	}
        }
    }

    var end = process.hrtime(start);
    var executionTime = (end[0] * 1000) + (end[1] / 1000000);

    //console.log(shortestPathNetwork);
    //console.log('Execution time: ' + executionTime + 'ms');

    var payload = {
        "layers": network.length,
        "shortestPath": shortestPath,
        "executionTime": executionTime + "ms",
        "network": network,
        "shortestPathNetwork": shortestPathNetwork,
    };

    if(event.queryStringParameters.generateGraph) {
        var AWS = require('aws-sdk');

        AWS.config.region = 'eu-west-1';

        var lambda = new AWS.Lambda();

        var params = {
            FunctionName: 'ConversionToNetworkGraph',
            InvocationType: 'RequestResponse',
            LogType: 'Tail',
            Payload: JSON.stringify(payload)
        };

        lambda.invoke(params, function(error, data) {
            if (error) {
                context.fail(error);
            }
            else {
                callback(null, {
                    statusCode: '200',
                    body: data.Payload,
                    headers: {
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*'
                    },
                });
            }
        })
    }
    else {
        callback(null, {
            statusCode: '200',
            body: JSON.stringify(payload),
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
        });
    }
};
