'use strict';

exports.handler = (event, context, callback) => {
	// Converting 2D array into a network graph for visualisation
    var graph = {
    	"type": "NetworkGraph",
    	"label": event.layers + " layers",
    	"metric": "shortest path = " + event.shortestPath + " (" + event.executionTime + ")",
    	"nodes": [],
    	"links": []
    };
    var network = event.network;
    var shortestPathNetwork = event.shortestPathNetwork;

    var nodeCount = 1;
    for(var i = 0; i < network.length; i++) {
    	var currentRow = network[i],
    		rowNumber = i + 1;

    	for(var j = 0; j < currentRow.length; j++) {
    		var currentNode = network[i][j];

    		// Populating nodes into the graph
    		graph.nodes.push({
                "id": nodeCount,
                "label": currentNode,
                "properties": {
                    "level": rowNumber,
                    "value": currentNode,
                    "shortest path": shortestPathNetwork[i][j]
                }
            });

            // Adding connections to node descendants, except for last row (last row has no descendants)
            if(i < (network.length - 1)) {
            	var lowerIndex = (rowNumber * (rowNumber + 1) / 2) + 1 + j

            	// Adding left connection
            	graph.links.push({
                	"source": nodeCount,
                	"target": lowerIndex,
                	"cost": 0
            	});

            	// Adding right connection
            	graph.links.push({
                	"source": nodeCount,
                	"target": lowerIndex + 1,
                	"cost": 0
            	});
            }

            nodeCount++;
	    }
    }

    callback(null, graph);
};
