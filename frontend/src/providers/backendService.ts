import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';

@Injectable()
export class BackendService {
    apiUrl: string = 'https://6yxjmt9d2g.execute-api.eu-west-1.amazonaws.com/prod/';
    apiKey: string = '8afLlMJdGw6ec1AeKtld73AkJFgSNvti42IbXGh7';

    constructor(public _http: Http) {
    }
    
    public generateNewGraph(numberOfLayers: number): Observable<any> {
        let headers = new Headers({'x-api-key': this.apiKey});
        let options = new RequestOptions({ headers: headers });
            
        return this._http.get(this.apiUrl + 'PyramidSliding?numberOfLayers=' + numberOfLayers + '&generateGraph=true', options)
            .map(this._extractData)
            .catch(this._handleError);
    }
    
    private _extractData(response: Response) {
        if (response.status < 200 || response.status >= 300) {
            throw new Error('Bad response status: ' + response.status);
        }
        return response.json() || {};
    }

    private _handleError(error: any): ErrorObservable {
        return Observable.throw(error);
    }
}