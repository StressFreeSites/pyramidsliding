import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, AlertController, LoadingController } from 'ionic-angular';

import { BackendService } from '../../providers/backendService';

declare var d3: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
    @ViewChild('graph') graph: ElementRef;
    
    constructor(public nav: NavController, public alert: AlertController, public loading: LoadingController, public backend: BackendService) {
    }
    
    ngOnInit() {
        this._generateNewGraph(5);
    }
    
    generateGraph() {
        let prompt = this.alert.create({
            title: 'New Graph',
            message: "Enter the number of layers for the pyramid, if you need more than 100 then please use the API directly.",
            inputs: [{
                name: 'numberOfLayers',
                placeholder: 'Number of Layers'
            }],
            buttons: [{
                text: 'Cancel',
            },{
                text: 'Generate',
                handler: data => {
                    this._generateNewGraph(data.numberOfLayers);
                }
            }]
        });
        prompt.present();
    }
    
    private _generateNewGraph(numberOfLayers) {
        let generatingLoader = this.loading.create({
            spinner: 'circles',
            showBackdrop: false,
            duration: 5000
        });
        generatingLoader.present();
        
        this.backend.generateNewGraph(numberOfLayers)
            .subscribe(
                data => {   
                    //console.log('data: ', data);
                    
                    d3.netJsonGraph(data, {
                        el: "#network-graph",
                        defaultStyle: false,
                        nodeClassProperty: "level",
                        gravity: 0.03,
                        labelDx: 15,
                        labelDy: 15
                    });
                },
                error => {
                    generatingLoader.dismiss();
                    
                    console.error(error);
                },
                () => {
                    generatingLoader.dismiss();
                    //console.log('Finished generateNewGraph');
                }
            );
    }
}