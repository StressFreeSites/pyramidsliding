# Pyramid Sliding

Pyramid Sliding is a fun sport,but you have to fast otherwise you'll get burnt. This app calculates the shortest path from the top to the bottom of the pyramid. 

# Demo

http://pyramidsliding.s3-website-eu-west-1.amazonaws.com/

The orange node is the top of the pyramid, and each pyramid layer is shown in a different colour. The shortest path from the top to bottom of the pyramid is stated at the bottom of the screen along with the time to execute. Note, to rearrange the network into a pyramid shape drag and hold the orange node to the edge of the screen. 

Click a node to see the shortest path from the top of the pyramid to the selected node.

# Backend Setup 

The backend is setup using AWS Lambda functions. Setup the files in the backend directory as Lambda functions within AWS.

# Frontend Setup

The frontend is setup using Ionic2, to get started do the following.

1. Run _npm install -g ionic_ to install Ionic
2. Run _npm install_ to install package dependancies
3. Run _ionic serve_ to load development version

Note, my default the frontend app will connect to the demo instance of AWS, re-configure the AWS credentials in the backandSerive.ts file to your own instance.
